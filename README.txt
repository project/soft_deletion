CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * How To Use
 * Contributors
 * Credits

INTRODUCTION
------------
    This module is for soft deletion of a node. There are some other modules
    for node soft deletion. But in 'Soft Deletion' there is a permission
    access to control content :-
    Access to put content into recycle-bin.
    Access to restore content from recycle-bin.
    Access to show soft deleted content.

INSTALLATION
------------
    Download and Enable Soft Deletion module. Go to permissions and grant
    the permission to a specific role.

    Requirements:
       Administration views

How To Use
------------
    Any user/role except administrator, who has permission to put content into
    recycle-bin can use delete link for soft deletion of content.
    (Go to /admin/content -> select 'delete' for soft delete).
    Click on menu: Recycle-Bin having url:/recycle-content to view Recycle-bin.
    Click on Restore link to get back the content from recycle-bin.

Similar Modules
---------------
    Entity Soft Delete : 'https://www.drupal.org/project/entity_soft_delete'
    Soft delete : 'https://www.drupal.org/project/soft_delete'

CREDITS
-------
 Current Maintainer: Parash Ram (https://www.drupal.org/u/parashram)
